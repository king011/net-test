#!/usr/bin/env bash

set -e

BashDir=$(cd "$(dirname $BASH_SOURCE)" && pwd)
eval $(cat "$BashDir/conf.sh")
if [[ "$Command" == "" ]];then
    Command="$0"
fi

function help(){
    echo "run project"
    echo
    echo "Usage:"
    echo "  $Command [flags]"
    echo
    echo "Flags:"
    echo "  -b, --build         build all before running"
    echo "  -c, --code          build go code before running"
    echo "  -h, --help          help for $Command"
}

ARGS=`getopt -o hbc --long help,build,code -n "$Command" -- "$@"`
eval set -- "${ARGS}"
build=0
code=0
while true
do
    case "$1" in
        -h|--help)
            help
            exit 0
        ;;
        -b|--build)
            build=1
            shift 1
        ;;
        -c|--code)
            code=1
            shift 1
        ;;
        --)
            shift
            break
        ;;
        *)
            echo Error: unknown flag "$1" for "$Command"
            echo "Run '$Command --help' for usage."
            exit 1
        ;;
    esac
done

if [[ $build != 0 ]];then
    "$BashDir/go.sh"
else
    if [[ $code != 0 ]];then
        "$BashDir/go.sh"
    fi
fi
cd "$Dir/bin"
args=(
    ./"$Target" server
)
exec="${args[@]}"
echo $exec
eval "$exec"

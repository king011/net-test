package cmd

import (
	"fmt"
	"io"
	"log"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"github.com/spf13/cobra"
)

func clientRequest(c net.Conn, msg []byte) (e error) {
	_, e = c.Write(msg)
	if e != nil {
		return
	}
	c.SetReadDeadline(time.Now().Add(time.Second * 5))
	_, e = io.ReadAtLeast(c, msg, len(msg))
	return
}
func init() {
	var (
		addr            string
		conn            int
		message, length int
	)

	cmd := &cobra.Command{
		Use:   `client`,
		Short: `run as client`,
		Run: func(cmd *cobra.Command, args []string) {
			now := time.Now()
			var (
				wait            sync.WaitGroup
				success, errors int32
			)
			for i := 0; i < conn; i++ {
				wait.Add(1)
				go func() {
					c, e := net.Dial(`tcp`, addr)
					if e == nil {
						msg := make([]byte, length)
						for i := 0; i < message; i++ {
							e = clientRequest(c, msg)
							if e != nil {
								log.Println(e)
								break
							}
						}
						c.Close()
					} else {
						log.Println(e)
					}
					wait.Done()
					if e == nil {
						atomic.AddInt32(&success, 1)
					} else {
						atomic.AddInt32(&errors, 1)
					}

				}()
			}
			wait.Wait()
			fmt.Printf(`conn: %v, success: %v, error: %v
message: %v,length: %v
duration: %v
`,
				conn, success, errors,
				message, length,
				time.Since(now),
			)
		},
	}
	flags := cmd.Flags()

	flags.StringVarP(&addr, `addr`,
		`a`,
		`127.0.0.1:9000`,
		`listen address`,
	)
	flags.IntVarP(&conn, `conn`,
		`c`,
		10000,
		`conn num`,
	)
	flags.IntVarP(&message, `message`,
		`m`,
		100,
		`message count`,
	)
	flags.IntVarP(&length, `len`,
		`l`,
		1024*4,
		`message length`,
	)
	rootCmd.AddCommand(cmd)
}

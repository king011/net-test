package cmd

import (
	"io"
	"log"
	"net"
	"sync"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	var (
		addr string
	)
	// ch := make(chan []byte, 1000)

	cmd := &cobra.Command{
		Use:   `server`,
		Short: `run as server`,
		Run: func(cmd *cobra.Command, args []string) {
			l, e := net.Listen(`tcp`, addr)
			if e != nil {
				log.Fatalln(e)
			}
			log.Println(`work at`, addr)
			defer l.Close()
			var tempDelay time.Duration // how long to sleep on accept failure
			var p sync.Pool
			p.New = func() interface{} {
				return make([]byte, 1024)
			}
			if e != nil {
				log.Fatalln(e)
			}
			log.Println("ready")
			for {
				c, e := l.Accept()
				if e != nil {
					if tempDelay == 0 {
						tempDelay = 5 * time.Millisecond
					} else {
						tempDelay *= 2
					}
					if max := 1 * time.Second; tempDelay > max {
						tempDelay = max
					}
					log.Printf("http: Accept error: %v; retrying in %v\n", e, tempDelay)
					time.Sleep(tempDelay)
					continue
				}
				tempDelay = 0

				go func(c net.Conn) {
					var (
						e error
						n int
						// buf []byte
					)
					// select {
					// case buf = <-ch:
					// default:
					// 	buf = make([]byte, 1024)
					// }
					buffer := p.New()
					buf := buffer.([]byte)
					for {
						n, e = c.Read(buf)
						if e != nil {
							break
						}
						_, e = c.Write(buf[:n])
						if e != nil {
							break
						}
					}
					if e != io.EOF {
						log.Println(e)
					}
					c.Close()
					p.Put(buffer)
					// select {
					// case ch <- buf:
					// default:
					// }

				}(c)
			}
		},
	}
	flags := cmd.Flags()

	flags.StringVarP(&addr, `addr`,
		`a`,
		`:9000`,
		`listen address`,
	)
	rootCmd.AddCommand(cmd)
}

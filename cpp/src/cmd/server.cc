#include "cmd/cmd.h"
#include "utils/utils.h"
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/fiber/all.hpp>
#include <boost/fiber/algo/work_stealing.hpp>
#define BUFFER_SIZE (1024 * 32)
typedef boost::asio::io_context io_context_t;
typedef boost::asio::ip::tcp::acceptor acceptor_t;
typedef boost::asio::ip::tcp::socket socket_t;
typedef boost::fibers::fiber fiber_t;
template <typename Mutex>
class io_round_robin_pool_t final
{
private:
    typedef Mutex mutex_t;
    std::vector<io_context_t> contexts_;
    std::vector<io_context_t::work> works_;
    std::vector<std::thread> threads_;
    std::size_t next_;
    mutex_t mutex_;

public:
    explicit io_round_robin_pool_t(std::size_t concurrency = std::thread::hardware_concurrency())
        : contexts_(concurrency > 0 ? concurrency : 1), next_(0)
    {
    }
    io_round_robin_pool_t(const io_round_robin_pool_t &) = delete;
    io_round_robin_pool_t &operator=(const io_round_robin_pool_t &) = delete;
    ~io_round_robin_pool_t()
    {
        works_.clear();
        for (auto &context : contexts_)
        {
            context.stop();
        }
        for (auto &thread : threads_)
        {
            if (thread.joinable())
            {
                thread.join();
            }
        }
        threads_.clear();
    }
    void run()
    {
        std::lock_guard<mutex_t> lock(mutex_);
        if (!works_.empty())
        {
            return;
        }
        for (auto &context : contexts_)
        {
            works_.push_back(io_context_t::work(context));
            threads_.emplace_back([](io_context_t &context) noexcept
                                  {
                                      boost::system::error_code ec;
                                      context.run(ec);
                                      if (ec)
                                      {
                                          std::cerr << "context run error ->  value" << ec.value() << ",message: " << ec.message() << std::endl;
                                      }
                                  },
                                  std::ref(context));
        }
    }
    void stop()
    {
        std::lock_guard<mutex_t> lock(mutex_);
        works_.clear();
        for (auto &context : contexts_)
        {
            context.stop();
        }
    }
    void join()
    {
        std::vector<std::thread> threads;
        {
            std::lock_guard<mutex_t> lock(mutex_);
            threads.swap(threads_);
        }
        for (auto &thread : threads)
        {
            if (thread.joinable())
            {
                thread.join();
            }
        }
        threads.clear();
    }
    io_context_t &get_context()
    {
        std::lock_guard<mutex_t> lock(mutex_);
        auto index = next_++;
        if (next_ >= contexts_.size())
        {
            next_ = 0;
        }
        return contexts_[index];
    }
};
template <typename Mutex>
void accept_routine(io_round_robin_pool_t<Mutex> &pool, acceptor_t &acceptor, boost::asio::yield_context yield)
{
    for (;;)
    {
        try
        {
            // 接受一個新連接
            auto &context = pool.get_context();
            socket_t s(context);
            acceptor.async_accept(s, yield);
            // 啓動 socket 協程
            boost::asio::spawn(context, [s = boost::move(s)](boost::asio::yield_context yield) mutable
                               {
                                   auto remote = s.remote_endpoint();
                                   try
                                   {
                                       char buffer[BUFFER_SIZE];
                                       for (;;)
                                       {
                                           // 讀取
                                           size_t length = s.async_read_some(
                                               boost::asio::buffer(buffer, BUFFER_SIZE),
                                               yield);
                                           // 寫入
                                           s.async_write_some(
                                               boost::asio::buffer(buffer, length),
                                               yield);
                                       }
                                   }
                                   catch (const boost::system::system_error &e)
                                   {
                                       //    std::cout << boost::diagnostic_information(e) << std::endl;
                                   }
                               });
        }
        catch (const std::bad_alloc &e)
        {
            std::cout << e.what() << std::endl;
        }
        catch (const boost::system::system_error &e)
        {
            std::cout << boost::diagnostic_information(e) << std::flush;
        }
    }
}

template <typename Mutex>
class server_t
{
private:
    io_round_robin_pool_t<Mutex> &pool_;
    acceptor_t &acceptor_;

public:
    server_t(io_round_robin_pool_t<Mutex> &pool, acceptor_t &acceptor) : pool_(pool), acceptor_(acceptor)
    {
        post();
    }
    ~server_t() {}

private:
    void post()
    {
        auto &ctx = pool_.get_context();
        auto s = std::make_shared<socket_t>(ctx);
        acceptor_.async_accept(*s, std::bind(&server_t::post_hadler, this, s, std::placeholders::_1));
    }
    void post_hadler(std::shared_ptr<socket_t> s, boost::system::error_code ec)
    {
        post();
        if (ec)
        {
            return;
        }
        auto buffer = std::make_shared<std::vector<uint8_t>>(BUFFER_SIZE);
        read(s, buffer);
    }
    void read(std::shared_ptr<socket_t> s, std::shared_ptr<std::vector<uint8_t>> buffer)
    {
        s->async_read_some(boost::asio::buffer(buffer->data(), buffer->size()),
                           std::bind(
                               &server_t::read_hadler, this,
                               s, buffer,
                               std::placeholders::_1, std::placeholders::_2));
    }
    void read_hadler(std::shared_ptr<socket_t> s, std::shared_ptr<std::vector<uint8_t>> buffer, boost::system::error_code ec, std::size_t size)
    {
        if (ec)
        {
            // std::cerr << ec.value() << ec.message() << std::endl;
            return;
        }
        write(s, buffer, size);
    }
    void write(std::shared_ptr<socket_t> s, std::shared_ptr<std::vector<uint8_t>> buffer, std::size_t size)
    {
        s->async_write_some(boost::asio::buffer(buffer->data(), size),
                            std::bind(
                                &server_t::write_hadler, this,
                                s, buffer,
                                std::placeholders::_1, std::placeholders::_2));
    }
    void write_hadler(std::shared_ptr<socket_t> s, std::shared_ptr<std::vector<uint8_t>> buffer, boost::system::error_code ec, std::size_t size)
    {
        if (ec)
        {
            return;
        }
        read(s, buffer);
    }
};
template <typename Mutex>
void fiber_accept(io_round_robin_pool_t<Mutex> &pool, acceptor_t &acceptor)
{
}
template <typename Mutex>
class fiber_server_t
{
private:
    io_round_robin_pool_t<Mutex> &pool_;
    acceptor_t &acceptor_;

public:
    fiber_server_t(io_round_robin_pool_t<Mutex> &pool, acceptor_t &acceptor) : pool_(pool), acceptor_(acceptor)
    {
    }
    ~fiber_server_t()
    {
    }
    void run()
    {
        for (;;)
        {
            try
            {
                // 接受一個新連接
                auto &context = pool_.get_context();
                socket_t s(context);
                async_accept(s);

                // 啓動 socket fiber
                fiber_t([slef = this, s = std::move(s)]() mutable
                        {
                            try
                            {
                                uint8_t buffer[BUFFER_SIZE];
                                for (;;)
                                {
                                    // 讀取
                                    size_t length = slef->async_read(s, buffer, BUFFER_SIZE);

                                    // 寫入
                                    slef->async_write(s, buffer, length);
                                }
                            }
                            catch (const boost::system::system_error &e)
                            {
                                //    std::cout << boost::diagnostic_information(e) << std::endl;
                            }
                        })
                    .detach();
            }
            catch (const std::bad_alloc &e)
            {
                std::cout << e.what() << std::endl;
            }
            catch (const boost::system::system_error &e)
            {
                std::cout << boost::diagnostic_information(e) << std::flush;
            }
        }
    }

private:
    size_t async_write(socket_t &s, uint8_t *buffer, size_t len)
    {
        boost::fibers::promise<size_t> promise;
        s.async_write_some(boost::asio::buffer(buffer, len), [&promise](boost::system::error_code ec, size_t size)
                           {
                               if (ec)
                               {
                                   auto e = boost::system::system_error(ec);
                                   promise.set_exception(std::make_exception_ptr(e));
                               }
                               else
                               {
                                   promise.set_value(size);
                               }
                           });
        return promise.get_future().get();
    }
    size_t async_read(socket_t &s, uint8_t *buffer, size_t len)
    {
        boost::fibers::promise<size_t> promise;
        s.async_read_some(boost::asio::buffer(buffer, len), [&promise](boost::system::error_code ec, size_t size)
                          {
                              if (ec)
                              {
                                  auto e = boost::system::system_error(ec);
                                  promise.set_exception(std::make_exception_ptr(e));
                              }
                              else
                              {
                                  promise.set_value(size);
                              }
                          });
        return promise.get_future().get();
    }

    void async_accept(socket_t &s)
    {
        boost::fibers::promise<void> promise;
        acceptor_.async_accept(s, [&promise](boost::system::error_code ec)
                               {
                                   if (ec)
                                   {
                                       auto e = boost::system::system_error(ec);
                                       promise.set_exception(std::make_exception_ptr(e));
                                   }
                                   else
                                   {
                                       promise.set_value();
                                   }
                               });

        return promise.get_future().get();
    }
};
template <typename Mutex>
void fiber_routine(io_round_robin_pool_t<Mutex> &pool, acceptor_t &acceptor)
{
    auto concurrency = std::thread::hardware_concurrency();
    for (size_t i = 0; i < concurrency; i++)
    {
        std::thread([&pool, &acceptor, concurrency]
                    {
                        boost::fibers::use_scheduling_algorithm<boost::fibers::algo::work_stealing>(concurrency, true);
                        fiber_server_t<Mutex> s(pool, acceptor);
                        fiber_t([&s]
                                { s.run(); })
                            .join();
                    })
            .detach();
    }
}
void subcommand_server(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("server", "run as server");

    cmd->callback(
        [&cmd = *cmd]
        {
            auto addr = cmd.get_option("--addr")->as<std::string>();
            auto mode = cmd.get_option("--mode")->as<std::string>();
            auto endpoint = get_endpoint(addr);
            if (mode == "async")
            {
                io_round_robin_pool_t<std::mutex> pool;
                auto &context = pool.get_context();
                acceptor_t acceptor(context, endpoint);
                std::cout << "async work at " << addr << std::endl;
                server_t<std::mutex> s(pool, acceptor);
                pool.run();
                pool.join();
            }
            else if (mode == "yield")
            {
                io_round_robin_pool_t<std::mutex> pool;
                auto &context = pool.get_context();
                acceptor_t acceptor(context, endpoint);
                std::cout << "yield work at " << addr << std::endl;
                boost::asio::spawn(context,
                                   std::bind(
                                       accept_routine<std::mutex>,
                                       std::ref(pool),
                                       std::ref(acceptor),
                                       std::placeholders::_1));
                pool.run();
                pool.join();
            }
            else if (mode == "fiber")
            {
                io_round_robin_pool_t<boost::fibers::mutex> pool;
                auto &context = pool.get_context();
                acceptor_t acceptor(context, endpoint);
                std::cout << "fiber work at " << addr << std::endl;
                fiber_routine(pool, acceptor);
                pool.run();
                pool.join();
            }
            else
            {
                throw std::runtime_error("not supported mode");
            }
            throw CLI::Success();
        });
    // 設置 參數
    std::string addr = ":9000";
    cmd->add_option("-a,--addr",
                    addr, // 默認 值
                    "listen addr",
                    true // true 使用 默認值
    );
    std::string mode = "async";
    cmd->add_option("-m,--mode",
                    mode,
                    "work mode [async yield fiber]",
                    true);
}

#include "cmd/cmd.h"
#include <boost/lexical_cast.hpp>

void subcommand_client(CLI::App &app)
{
    CLI::App *cmd = app.add_subcommand("client", "run as client");

    cmd->callback(
        [&cmd = *cmd]
        {
            const std::string addr = cmd.get_option("--addr")->as<std::string>();
        });
    // 設置 參數
    std::string addr = ":8000";
    cmd->add_option("-a,--addr",
                    addr, // 默認 值
                    "listen addr",
                    true // true 使用 默認值
    );
}

#include "utils/utils.h"
#include <boost/lexical_cast.hpp>
boost::asio::ip::tcp::endpoint get_endpoint(const std::string &addr)
{
    auto find = addr.find_last_of(":");
    if (find == std::string::npos)
    {
        throw std::domain_error("bad addr");
    }
    auto port = boost::lexical_cast<boost::asio::ip::port_type>(addr.substr(find + 1));
    auto domain = addr.substr(0, find);
    if (domain.empty())
    {
        return boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v6(), port);
    }
    else
    {
        return boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(domain), port);
    }
}
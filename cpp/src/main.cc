#include <iostream>
#include "utils/utils.h"
#include "CLI11.hpp"
#include "version.h"
#include "cmd/cmd.h"
int main(int argc, char *argv[])
{
    // 定義 命令
    CLI::App app("cpp-net-test");

    // 設置 命令 處理 回調
    app.callback(
        [&cmd = app]
        {
            // 獲取 參數
            const CLI::Option &v = *cmd.get_option("--version");
            if (v)
            {
                std::cout << CPP_NET_TEST_VERSION
                          << std::endl;
            }
            else
            {
                // 打印 命令使用 說明
                // std::cerr << cmd.help() << std::endl;

                // 通知 CLI11_PARSE 異常
                // CallForHelp 被捕獲 會自動 調用 help()
                throw CLI::CallForHelp();
            }
        });
    // 允許輸入 額外參數
    //app.allow_extras();

    // 爲命令 定義 參數
    app.add_flag("-v,--version", "display version");

    // 添加子命令
    subcommand_server(app);
    subcommand_client(app);

    // 解析 命令 並 執行 同時 捕獲 CLI::ParseError 異常
    CLI11_PARSE(app, argc, argv);
    return 0;
}
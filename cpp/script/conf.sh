Target="cpp-net-test"
Docker="cpp-net-test"
Macro="CPP_NET_TEST"
Dir=$(cd "$(dirname $BASH_SOURCE)/.." && pwd)
Version="v0.0.1"
Platforms=(
    windows/amd64
    linux/amd64
)
#ifndef __CPP_NET_TEST_CMD_CMD_H__
#define __CPP_NET_TEST_CMD_CMD_H__
#include "../CLI11.hpp"

void subcommand_client(CLI::App &app);
void subcommand_server(CLI::App &app);
#endif // __CPP_NET_TEST_CMD_CMD_H__

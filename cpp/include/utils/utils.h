#ifndef __CPP_NET_TEST_UTILS_UTILS_H__
#define __CPP_NET_TEST_UTILS_UTILS_H__
#include <boost/asio.hpp>
const char *systemos();
boost::asio::ip::tcp::endpoint get_endpoint(const std::string &addr);
#endif // __CPP_NET_TEST_UTILS_UTILS_H__